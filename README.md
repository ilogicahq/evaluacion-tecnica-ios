# Evaluación técnica iOS

## Introducción
Hola!!!!!

Primero, muchas gracias por postular al cargo de Desarrollador iOS, ahora queremos saber tus capacidades técnicas con un ejercicio, para esto necesitaremos que crees una aplicación con ciertas caracteristicas y que interactuara con una API REST que hemos disponibilizado, el archivo `end-points-postman.json` del repositorio puedes importarlo a postman y podras tener ejemplos del funcionamiento de la API, cualquier duda escribenos a jaime@ilogica.cl o jacevedo@ilogica.cl.


## Ejercicio
Debes crear una aplicación en iOS que tenga las siguientes caracteristicas:
- Crear personas por medio de un formulario.
- Listar el total de personas existentes.
- Podras borrar una persona, donde tu estimes que sea conveniente.
- Deberás crear grupos de personas, como estimes conveniente de forma automática o manual, cualquiera es válida.
- Podras Listar los grupos.
- Podras eliminar un grupo, tambien donde estimes conveniente.

Todo lo anterior debera ser siempre interactuando con la API.

## End points

### En todos los requests deberas enviar tu `nombre apellido, idealmente por medio de un campo llamado `postulant`, esto para poder indentificar los registros realizados.

## URL base: `https://us-central1-prueba-tecnica-bbfaf.cloudfunctions.net`

### **POST** - `/addPerson`
Creas una persona.

##### Body del request (json):

```json
{
	"postulant":"Jaime Acevedo", 
	"name":"Jaime", 
	"lastName":"Acevedo",
	"isVegan":true, 
	"isCarnivorous":false,
	"charge":"Developer"
}
```

##### Detalle del json:
+ postulant: `Nombre Apellido` (string, required) - Tu nombre y appellido separado por un espacio
+ name: `Pedro` (string, required) - Nombre de la persona
+ lastName: `Perez` (string, required) - Apellido de la persona
+ isVegan: `true|false` (boolean, required) - Es vegano o no.
+ isCarnivorous: `true|false` (boolean, required) - Es carnivoro o no.
+ charge: `Developer` (string, required) - Define like reaction type.

#### Respuesta de la API

```json
{
    "status": "ok"
}
            
```
##### Detalle de respuesta json:
+ status: `ok`, fue ingresado correctamente.

***

### **POST** - `/getPersons`
Obtienes las personas basadas en el postulante que las ingreso

##### Parametros de la URL:
+ postulant: `jaime_acevedo` (string, required) - nombre del postulante en minusculas sin caracteres especiales, espacio se cambia por `_`
#### Respuesta de la API

```json
{
    "status": "ok",
    "data": [
        {
            "isVegan": true,
            "charge": "Developer",
            "lastName": "Acevedo",
            "name": "Jaime",
            "isCarnivorous": false
        },
        {
            "isVegan": false,
            "charge": "Developer",
            "lastName": "pablo",
            "name": "Pedro",
            "isCarnivorous": true
        }
    ]
}        
```

***

### **POST** - `/removePerson`
Elimina una persona por su campo `name`

##### Body del request (json):

```json
{
	"postulant":"Jaime Acevedo",
	"name":"Jaime"
}
```
##### Detalle del json:
+ postulant: `Nombre Apellido` (string, required) - Tu nombre y appellido separado por un espacio
+ name: `Pedro` (string, required) - Nombre de la persona

#### Respuesta de la API

```json
{
    "status": "ok"
}
            
```
##### Detalle de respuesta json:
+ status: `ok`, fue ingresado correctamente.

***

### **POST** - `/saveGroup`
Con este endpoint creas grupo

##### Body del request (json):

```json
{
	"postulant":"Jaime Acevedo",
	"nameGroup":"Grupo 1",
	"persons":["Jaime"]
}
```

##### Detalle del json:
+ postulant: `Nombre Apellido` (string, required) - Tu nombre y appellido separado por un espacio
+ nameGroup: `Grupo 1` (string, required) - Nombre del grupo
+ persons: [`Jaime`] (Array[string], required) - Arreglo con campo `name` de las personas ingresada anteriormente que se quieren asignar al grupo

#### Respuesta de la API

```json
{
    "status": "ok"
}
            
```
##### Detalle de respuesta json:
+ status: `ok`, fue ingresado correctamente.

***

### **POST** - `/getGroups`
Obtienes los grupos basado en el postulante que las ingreso

##### Parametros de la URL:
+ postulant: `jaime_acevedo` (string, required) - nombre del postulante en minusculas sin caracteres especiales, espacio se cambia por `_`
#### Respuesta de la API

```json
{
    "status": "ok",
    "data": [
        {
            "name": "grupo_1",
            "members": [
                {
                    "lastName": "pablo",
                    "name": "Pedro",
                    "isCarnivorous": true,
                    "isVegan": false,
                    "charge": "Developer"
                }
            ]
        },
        {
            "name": "grupo_2"
        }
    ]
}      
```

***


### **POST** - `/deleteGroup`
Elimina un grupo basado en su campo `name`

##### Body del request (json):

```json
{
	"postulant":"Jaime Acevedo",
	"name":"Jaime"
}
```
##### Detalle del json:
+ postulant: `Nombre Apellido` (string, required) - Tu nombre y appellido separado por un espacio
+ name: `Pedro` (string, required) - Nombre de la persona

#### Respuesta de la API

```json
{
    "status": "ok"
}
            
```
##### Detalle de respuesta json:
+ status: `ok`, fue ingresado correctamente.

***